#include "Game.h"
#include <iostream>
#include <string>
using namespace std;

int Game::Init()
{
	if (!sdl.Init())
		return 1;

	if (!sdl.CreateWindow("Shader Effects", 800, 600))
		return 2;

	if (!sdl.InitOpenGL(3, 3))
	{
		return 3;
	}

	const GLubyte* versionString = glGetString(GL_VERSION);

	cout << "OpenGL version string: " << versionString << endl;



	keystates = SDL_GetKeyboardState(nullptr);


	basicShader.LoadShaders("Shaders/basic.vert", "Shaders/basic.frag");

	glEnable(GL_TEXTURE_2D);

	// Make objects which are drawn later avoid drawing parts of themselves that are behind other objects
	glEnable(GL_DEPTH_TEST);


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glViewport(0, 0, sdl.GetW(), sdl.GetH());


	// Bind vertex array object (container for reusable geometry and its properties)
	vao.Init();
	vao.Bind();
	LoadGeometry();


	// Values for the basic shader
	basicShader.Activate();

	basicTimeUniform.LoadLocation(basicShader.GetProgram(), "time");


	// Get the location of the projectionMatrix uniform variable in the shader
	projectionMatrixLocation = glGetUniformLocation(basicShader.GetProgram(), "projectionMatrix");

	modelMatrixLocation = glGetUniformLocation(basicShader.GetProgram(), "modelMatrix");



	smileTexture.Load("Images/smile.png");
	smileTexture.UseClamp();

	return 0;
}

void Game::LoadGeometry()
{
	// top center
	triangle.AddVertex(Vector3(0.0f, 0.6f, 0.0f), Color(1.0f, 0.0f, 0.0f, 1.0f), TexCoord(0.5f, 0.0f));
	// bottom right
	triangle.AddVertex(Vector3(0.8f, -0.6f, 0.0f), Color(0.0f, 1.0f, 0.0f, 1.0f), TexCoord(1.0f, 1.0f));
	// bottom left
	triangle.AddVertex(Vector3(-0.8f, -0.6f, 0.0f), Color(0.0f, 0.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));


	// Bind a vertex buffer object to store our geometry data on the GPU
	triangleVBO.Init();
	triangleVBO.Bind();
	// Upload to the GPU
	triangleVBO.Upload(triangle);
}


/*float deleteThis[16] = {1, 0, 0, 0,
						  0, 1, 0, 0,
						  0, 0, 1, 0,
						  0, 0, 0, 1};*/

void SetIdentity(float* matrix)
{
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			int index = row * 4 + col;
			if (row == col)
			{
				matrix[index] = 1;
			}
			else
			{
				matrix[index] = 0;
			}
		}
	}
}

void CopyMatrix(float* result, const float* source)
{

}

void SetScale(float* result, float sx, float sy, float sz)
{

}


/*float deleteThis[16] = {1, 0, 0, 0,
						  0, 1, 0, 0,
						  0, 0, 1, 0,
						  x, y, z, 1};*/
void SetTranslate(float* result, float x, float y, float z)
{
	SetIdentity(result);
	result[12] = x;
	result[13] = y;
	result[14] = z;
}

void SetRotateX(float* result, float degrees)
{

}

void SetRotateY(float* result, float degrees)
{

}

void SetRotateZ(float* result, float degrees)
{

}

// Matrix multiply: B = A * B
// Note: A temporary copy of B is necessary for this function because B is changing while still being used!
void MultiplyIntoRight(const float* A, float* B)
{
	float tempB[16];
	CopyMatrix(tempB, B);

}



void SetOrtho(float* matrix, float left, float right, float bottom, float top, float zNear, float zFar)
{

}


int Game::Run()
{
	gameMode = 0;

	done = false;

	SDL_Event event;
	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			HandleEvent(event);
		}

		Update();

		Draw();

		SDL_Delay(1);
	}

	return 0;
}


void Game::HandleEvent(const SDL_Event& event)
{
	if (event.type == SDL_QUIT)
	{
		done = true;
	}
	else if (event.type == SDL_KEYDOWN)
	{
		if (event.key.keysym.sym == SDLK_ESCAPE)
		{
			done = true;
		}
		else if (event.key.keysym.sym == SDLK_r)
		{
			basicShader.ReloadShaders();
		}
		else if (event.key.keysym.sym == SDLK_SPACE)
		{
			gameMode++;
			if (gameMode > 3)
				gameMode = 0;
		}
	}
}


void Game::Update()
{
	SetIdentity(projectionMatrix);
}

void Game::Draw()
{
	// Do some rendering!
	glClearColor(0.1f, 0.2f, 0.4f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	basicShader.Activate();

	// Uniforms for basic shader
	basicTimeUniform.Set(SDL_GetTicks() / 1000.0f);
	basicTimeUniform.Update();
	// Drawing objects with basic shader
	smileTexture.Bind();

	glDisable(GL_BLEND);
	triangleVBO.Bind();

	glUniformMatrix4fv(projectionMatrixLocation, 1, false, projectionMatrix);

	if (gameMode == 0)
	{
		SetIdentity(modelMatrix);

		glUniformMatrix4fv(modelMatrixLocation, 1, false, modelMatrix);

		triangle.Draw();
	}
	else if (gameMode == 1)
	{
		SetIdentity(modelMatrix);

		float t = sin(SDL_GetTicks() / 1000.0f);
		SetScale(modelMatrix, t, t, t);

		glUniformMatrix4fv(modelMatrixLocation, 1, false, modelMatrix);

		triangle.Draw();
	}
	else if (gameMode == 2)
	{
		SetIdentity(modelMatrix);

		float rotationX[16];
		float rotationY[16];
		SetRotateX(rotationX, 30 * SDL_GetTicks() / 1000.0f);
		SetRotateY(rotationY, 20 * SDL_GetTicks() / 1000.0f);

		MultiplyIntoRight(rotationY, modelMatrix);
		MultiplyIntoRight(rotationX, modelMatrix);

		glUniformMatrix4fv(modelMatrixLocation, 1, false, modelMatrix);

		triangle.Draw();
	}
	else if (gameMode == 3)
	{
		SetIdentity(modelMatrix);

		float rotationX[16];
		float rotationY[16];
		SetRotateX(rotationX, 30 * SDL_GetTicks() / 1000.0f);
		SetRotateY(rotationY, 20 * SDL_GetTicks() / 1000.0f);

		float translation[16];
		SetTranslate(translation, 0, 0, 0.5f);


		// Draw the first triangle
		MultiplyIntoRight(rotationY, modelMatrix);
		MultiplyIntoRight(rotationX, modelMatrix);

		glUniformMatrix4fv(modelMatrixLocation, 1, false, modelMatrix);

		triangle.Draw();



		// Draw another!
		SetIdentity(modelMatrix);

		MultiplyIntoRight(translation, modelMatrix);
		MultiplyIntoRight(rotationY, modelMatrix);
		MultiplyIntoRight(rotationX, modelMatrix);

		glUniformMatrix4fv(modelMatrixLocation, 1, false, modelMatrix);

		triangle.Draw();
	}


	glEnable(GL_BLEND);


	sdl.SwapWindow();
}