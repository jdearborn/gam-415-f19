#pragma once
#include "SDLObjects.h"
#include "GLObjects.h"
#include <string>
#include <vector>
#include "Random.h"



class Game
{
public:
	SDL sdl;

	Random rnd;

	Projection projection;

	VertexArrayObject vao;

	Geometry triangle;
	VertexBufferObject triangleVBO;

	Geometry quad;
	VertexBufferObject quadVBO;

	Geometry bigQuad;
	VertexBufferObject bigQuadVBO;

	ShaderProgram basicShader;
	ShaderProgram cloudShader;
	ShaderProgram warpShader;

	int Init();

	void LoadGeometry();

	int Run();
};
