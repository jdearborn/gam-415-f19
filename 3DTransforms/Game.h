#pragma once
#include "SDLObjects.h"
#include "GLObjects.h"
#include <string>
#include <vector>
#include "Random.h"



class Game
{
public:
	SDL sdl;

	Random rnd;



	VertexArrayObject vao;

	Geometry triangle;
	VertexBufferObject triangleVBO;

	ShaderProgram basicShader;



	// For quitting the game
	bool done;

	const Uint8* keystates;

	Uniform<float> basicTimeUniform;
	GLint projectionMatrixLocation;
	GLint modelMatrixLocation;

	int gameMode;
	float projectionMatrix[16];
	float modelMatrix[16];

	Texture smileTexture;




	int Init();

	void LoadGeometry();

	int Run();

	void HandleEvent(const SDL_Event& event);

	void Update();

	void Draw();
};
