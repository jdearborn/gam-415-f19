#pragma once

#include "glew.h"
#include <string>
#include <vector>


struct Vector3
{
public:
	float x, y, z;

	Vector3()
		: x(0.0f), y(0.0f), z(0.0f)
	{}
	Vector3(float x, float y, float z)
		: x(x), y(y), z(z)
	{}
};

struct Color
{
public:
	float r, g, b, a;

	Color()
		: r(1.0f), g(1.0f), b(1.0f), a(1.0f)
	{}
	Color(float r, float g, float b)
		: r(r), g(g), b(b), a(1.0f)
	{}
	Color(float r, float g, float b, float a)
		: r(r), g(g), b(b), a(a)
	{}
};

struct TexCoord
{
public:
	float u, v;

	TexCoord()
		: u(0.0f), v(0.0f)
	{}
	TexCoord(float u, float v)
		: u(u), v(v)
	{}
};


// Buffer format: x, y, z, r, g, b, a, u, v
class Geometry
{
public:

	Geometry();

	void AddVertex(const Vector3& position, const Color& color, const TexCoord& texCoord);

	std::vector<float>& GetBuffer();

	void Draw();

private:
	GLenum primitive;
	int stride;
	std::vector<float> vertexBuffer;
};

class VertexArrayObject
{
public:
	VertexArrayObject();
	~VertexArrayObject();

	void Init();

	void Free();

	void Bind();

private:
	GLuint vao;

	// Explicitly disable copying this object
	VertexArrayObject(const VertexArrayObject& other) = delete;
	VertexArrayObject& operator=(const VertexArrayObject& other) = delete;
};

class VertexBufferObject
{
public:
	VertexBufferObject();
	~VertexBufferObject();

	void Init();

	void Free();

	void Bind();

	void Upload(Geometry& geometry);

private:
	GLuint vbo;
};

class ShaderProgram
{
public:
	ShaderProgram();

	~ShaderProgram();

	void FreeShaders();

	bool LoadShaders(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);

	bool ReloadShaders();

	void Activate();

	GLuint GetProgram();

private:
	std::string vertexFile, fragmentFile;
	GLuint vertexShader, fragmentShader;
	GLuint program;

	// Returns the entire file loaded into a single string
	std::string LoadFileString(const std::string& filename);

	bool CompileShader(const std::string& shaderName, GLuint shader);
};




class Texture
{
public:

	Texture();
	~Texture();

	bool Load(const std::string& filename);

	void Free();

	void Bind();

	void UseRepeat();

	void UseClamp();

private:
	GLuint tex;

	// Explicitly disable copying this object
	Texture(const Texture& other) = delete;
	Texture& operator=(const Texture& other) = delete;
};

class Projection
{
public:
	Projection();

	void SetIdentity();
	void SetOrtho(float left, float right, float bottom, float top, float zNear, float zFar);

	float* GetMatrix();

private:
	float matrix[16];
};


template<typename T>
class Uniform
{
public:

	Uniform()
		: values(nullptr), size(0), program(0), location(-1)
	{}
	~Uniform()
	{
		delete[] values;
	}
	Uniform(const Uniform& other)  // Copy constructor
		: values(nullptr), size(0), program(0), location(-1)
	{
		*this = other;  // Reuse the copy assignment operator
	}
	Uniform& operator=(const Uniform& other)  // Copy assignment
	{
		delete[] values;

		program = other.program;
		location = other.location;
		if (other.values == nullptr)
		{
			values = nullptr;
			size = 0;
		}
		else
		{
			size = other.size;
			values = new T[size];
			memcpy(values, other.values, size * sizeof(T));
		}

		return *this;
	}

	void LoadLocation(GLuint program, const std::string& name)
	{
		this->name = name;
		this->program = program;
		location = glGetUniformLocation(program, name.c_str());
	}

	void Reload()
	{
		LoadLocation(program, name);
	}

	void Set(const T& v)
	{
		if (size != 1)
		{
			delete[] values;
			values = new T(v);
			size = 1;
		}
		else
		{
			values[0] = v;
		}
	}

	void Set(const T* newValues, int size)
	{
		if (this->size != size)
		{
			delete[] values;
			values = new T[size];
			this->size = size;
		}
		memcpy(values, newValues, size * sizeof(T));
	}

	// To be specialized for float
	void Set(const Vector3& v);

	// To be specialized
	void Update();

private:

	T* values;
	int size;

	GLuint program;
	GLint location;
	std::string name;
};


template<>
inline void Uniform<float>::Set(const Vector3& v)
{
	Set(&v.x, 3);
}

template<>
inline void Uniform<float>::Update()
{
	switch (size)
	{
	case 0:
		return;
	case 1:
		glUniform1f(location, values[0]);
		break;
	case 2:
		glUniform2f(location, values[0], values[1]);
		break;
	case 3:
		glUniform3f(location, values[0], values[1], values[2]);
		break;
	case 4:
		glUniform4f(location, values[0], values[1], values[2], values[3]);
		break;
	case 9:  // 3x3 matrix
		glUniformMatrix3fv(location, 1, false, values);
		break;
	case 16:  // 4x4 matrix
		glUniformMatrix4fv(location, 1, false, values);
		break;
	default:
		return;
	}
}

template<>
inline void Uniform<int>::Update()
{
	switch (size)
	{
	case 0:
		return;
	case 1:
		glUniform1i(location, values[0]);
		break;
	case 2:
		glUniform2i(location, values[0], values[1]);
		break;
	case 3:
		glUniform3i(location, values[0], values[1], values[2]);
		break;
	case 4:
		glUniform4i(location, values[0], values[1], values[2], values[3]);
		break;
	default:
		return;
	}
}