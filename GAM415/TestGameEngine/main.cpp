#include <iostream>
#include "GameEngine.h"
#include <functional>
#include <vector>
#include <algorithm>
#include <cassert>
using namespace std;




class PlayerController : public Component
{
public:
	PlayerController()
	{
		AddType(this);
	}

	void Update() override
	{
		Transform* transform = owner->GetComponent<Transform>();
		bool didPressDirection = false;
		if (Input::GetKey(KeyCode::W))
		{
			transform->Translate(0, 1, 0);
			didPressDirection = true;
		}
		if (Input::GetKey(KeyCode::S))
		{
			transform->Translate(0, -1, 0);
			didPressDirection = true;
		}
		if (Input::GetKey(KeyCode::A))
		{
			transform->Translate(-1, 0, 0);
			didPressDirection = true;
		}
		if (Input::GetKey(KeyCode::D))
		{
			transform->Translate(1, 0, 0);
			didPressDirection = true;
		}

		if (didPressDirection)
		{
			cout << "Player position: "
				<< transform->position.x << ", "
				<< transform->position.y << ", "
				<< transform->position.z << endl;
		}
	}
};


int Print5(int a, int b)
{
	cout << 5 << endl;
	return 0;
}

void DoSomethingWithTwoIntegers(std::function<int(int, int)> fn, int a, int b)
{
	if (fn != nullptr)
		fn(a, b);
}


// Register with OnKeyDown
void QuitWhenYouPressEscape(KeyCode key, KeyModifier mod)
{
	if (key == KeyCode::Escape)
	{
		GameEngine::Quit();
	}
}

void PrintKeyDown(KeyCode key, KeyModifier mod)
{
	cout << "You pressed: " << (int)key << endl;
}

void PrintKeyUp(KeyCode key, KeyModifier mod)
{
	cout << "You released: " << (int)key << endl;
}




void RunScene()
{
	// Set up event callbacks
	GameEngine::OnQuit += []() {
		cout << "Goodbye!" << endl;
	};

	GameEngine::OnKeyDown += PrintKeyDown;
	GameEngine::OnKeyUp += PrintKeyUp;
	GameEngine::OnKeyDown += QuitWhenYouPressEscape;


	// Create a player object and attach the needed components
	Entity* obj = Entity::Instantiate();
	PlayerController* player = obj->AddComponent<PlayerController>();


	obj->AddComponent<Transform>();
	SpriteRenderer* renderer = obj->AddComponent<SpriteRenderer>();
	Texture t;
	t.Load("Images/smile.png");
	renderer->SetTexture(&t);




	// Start the main loop
	GameEngine::Run();

	GameEngine::OnQuit.Clear();
	GameEngine::OnKeyDown.Clear();
	GameEngine::OnUpdate.Clear();
	GameEngine::OnKeyUp.Clear();

	GameEngine::objectManager.Clear();
	GameEngine::renderManager.Clear();
}


/*class MySingleton
{
public:
	int a;

	static MySingleton& Instance()
	{
		if (_instance == nullptr)
		{
			_instance = new MySingleton();
		}

		return *_instance;
	}

private:
	static MySingleton* _instance;

	MySingleton()
	{
		a = 15;
	}
};

MySingleton* MySingleton::_instance = nullptr;*/


/*void DoBadThings(int* ptr)
{
	assert(ptr != nullptr);
	*ptr = 5;
}*/

/*Ivan's faceroll*/
class Faceroll
{
public:
	static Faceroll* GetInstance()
	{
		static Faceroll* instance = nullptr;
		if (instance == nullptr)
		{
			instance = new Faceroll();
		}
		return instance;
	}

private:
	Faceroll()
	{

	}

	Faceroll(const Faceroll& other) = delete;
};






int main(int argc, char* argv[])
{
	Window* window = GameEngine::Init("Welcome to Game.", 1280, 720);
	if (window == nullptr)
	{
		return 1;
	}

	GameEngine::OnKeyDown += [window](KeyCode k, KeyModifier m) {
		if (k == KeyCode::Space)
		{
			window->SetTitle("You pressed SPACE");
		}
		else if (k == KeyCode::Enter)
		{
			window->SetTitle("You pressed ENTER");
		}
	};


	RunScene();


	// Clean up
	GameEngine::Deinit();

	return 0;
}