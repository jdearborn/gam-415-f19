#include "Game.h"

int main(int argc, char* argv[])
{
	Game game;
	int init = game.Init();
	if (init != 0)
		return init;

	return game.Run();
}