#version 330 core
in vec4 color;
in vec2 texCoords;
out vec4 outColor;

uniform sampler2D tex;
uniform float time;

void main()
{
	float t = time;
	// Choose center of the pinch
	vec2 center = vec2(0.5, 0.5);
	vec2 centerOffset = texCoords - center;
	// The distance will affect the power of the effect
	float centerDistance = distance(texCoords, center);
	
	// Do a 2D rotation of the texture coords about the center
	float st = sin(centerDistance*t);
	float ct = cos(centerDistance*t);
	vec2 rotated = center + vec2(centerOffset.x * ct - centerOffset.y * st, centerOffset.x * st + centerOffset.y * ct);
	
	vec4 texColor = texture2D(tex, rotated);
	outColor = color * texColor;
}