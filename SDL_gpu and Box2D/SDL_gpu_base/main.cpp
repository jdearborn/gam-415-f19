#include <iostream>
#include "SDL_gpu.h"
#include "NFont_gpu.h"
#include "Box2D/Box2D.h"
using namespace std;



static void set_vertex(float* vertex_array, unsigned int vertex_index, unsigned short* index_array, unsigned int index_array_index, float x, float y, float s, float t, float r, float g, float b, float a)
{
	unsigned int n = vertex_index * 8;
	vertex_array[n++] = x;
	vertex_array[n++] = y;
	vertex_array[n++] = s;
	vertex_array[n++] = t;
	vertex_array[n++] = r;
	vertex_array[n++] = g;
	vertex_array[n++] = b;
	vertex_array[n++] = a;
	index_array[index_array_index++] = vertex_index;
}

static void set_index(unsigned short* index_array, unsigned int index_array_index, unsigned short index)
{
	index_array[index_array_index] = index;
}

static void DrawTexturedPolygon(GPU_Image* image, GPU_Target* target, unsigned int num_vertices, float* vertices_xy, float tex_x, float tex_y, float scale_x, float scale_y)
{
	if (num_vertices < 3)
		return;

	GPU_WrapEnum wrap_x = image->wrap_mode_x;
	GPU_WrapEnum wrap_y = image->wrap_mode_y;
	GPU_SetWrapMode(image, GPU_WRAP_REPEAT, GPU_WRAP_REPEAT);

	float* vertices = (float*)malloc(8 * num_vertices * sizeof(float));
	unsigned short* indices = (unsigned short*)malloc((3 + (num_vertices - 3) * 3) * sizeof(unsigned short));
	unsigned short vcount = 0;
	unsigned int icount = 0;

	unsigned int numSegments = 2 * num_vertices;
	float r, g, b, a;
	r = g = b = a = 255.0f;

	// Using a fan of triangles assumes that the polygon is convex
	unsigned int i = 0;

	// First triangle
	set_vertex(vertices, vcount++, indices, icount++, vertices_xy[i], vertices_xy[i + 1], (vertices_xy[i] - tex_x) * scale_x + 0.5f, (vertices_xy[i + 1] - tex_y) * scale_y + 0.5f, r, g, b, a);
	i += 2;
	set_vertex(vertices, vcount++, indices, icount++, vertices_xy[i], vertices_xy[i + 1], (vertices_xy[i] - tex_x) * scale_x + 0.5f, (vertices_xy[i + 1] - tex_y) * scale_y + 0.5f, r, g, b, a);
	i += 2;
	set_vertex(vertices, vcount++, indices, icount++, vertices_xy[i], vertices_xy[i + 1], (vertices_xy[i] - tex_x) * scale_x + 0.5f, (vertices_xy[i + 1] - tex_y) * scale_y + 0.5f, r, g, b, a);
	i += 2;

	unsigned short last_index = 2;

	while (i < numSegments)
	{
		set_index(indices, icount++, 0);  // Start from the first vertex
		set_index(indices, icount++, last_index);  // Double the last one
		set_vertex(vertices, vcount++, indices, icount++, vertices_xy[i], vertices_xy[i + 1], (vertices_xy[i] - tex_x) * scale_x + 0.5f, (vertices_xy[i + 1] - tex_y) * scale_y + 0.5f, r, g, b, a);
		i += 2;
		last_index++;
	}

	// Do the rendering
	GPU_PrimitiveBatch(image, target, GPU_TRIANGLES, vcount, vertices, icount, indices, GPU_BATCH_XY_ST_RGBA);

	// Restore wrap mode
	GPU_SetWrapMode(image, wrap_x, wrap_y);

	free(indices);
	free(vertices);
}

class GetBodyCallback : public b2QueryCallback
{
public:
	float x, y;
	b2Body* body;

	GetBodyCallback()
		: b2QueryCallback(), x(0.0f), y(0.0f), body(nullptr)
	{}
	GetBodyCallback(float x, float y)
		: b2QueryCallback(), x(x), y(y), body(nullptr)
	{}

	bool ReportFixture(b2Fixture* fixture) override
	{
		// A fixture's AABB overlaps with this one.  Check if the fixture's actual geometry overlaps.
		if (fixture->TestPoint(b2Vec2(x, y)))
		{
			body = fixture->GetBody();
			return false;  // Stop the query
		}
		return true;  // Continue the query
	}
};



int main(int argc, char* argv[])
{
	GPU_Target* screen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);
	if (screen == nullptr)
		return 1;

	SDL_SetWindowTitle(SDL_GetWindowFromID(screen->context->windowID), "My Awesome Game");

	GPU_Image* smileImage = GPU_LoadImage("smile.png");
	if (smileImage == nullptr)
		return 2;

	NFont font;
	font.load("FreeSans.ttf", 30);

	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	GPU_SetCoordinateMode(true);

	// Adapted from Box2D's HelloWorld.cpp
	float worldToScreenScale = 10;  // Relates pixels and meters:    pixels per meter

	// Define the gravity vector.
	b2Vec2 gravity(0.0f, -9.8f);

	// Construct a world object, which will hold and simulate the rigid bodies.
	b2World world(gravity);

	// Define the ground body.
	b2BodyDef groundDef;
	groundDef.type = b2_staticBody;
	groundDef.position.Set(10.0f, 20.0f);

	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	b2Body* groundBody = world.CreateBody(&groundDef);

	// Define the ground box shape.
	b2PolygonShape groundBox;

	// The extents are the half-widths of the box.
	groundBox.SetAsBox(500.0f, 10.0f);

	// Add the ground fixture to the ground body.
	groundBody->CreateFixture(&groundBox, 0.0f);

	// Define the dynamic body. We set its position and call the body factory.
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(10.0f, 60.0f);
	bodyDef.angle = 40 * ((float)M_PI / 180);
	b2Body* boxBody = world.CreateBody(&bodyDef);

	// Define another box shape for our dynamic body.
	b2PolygonShape dynamicBox;
	dynamicBox.SetAsBox(3.0f, 3.0f);

	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;

	// Set the box density to be non-zero, so it will be dynamic.
	fixtureDef.density = 1.0f;

	// Override the default friction.
	fixtureDef.friction = 0.3f;

	// Add the shape to the body.
	boxBody->CreateFixture(&fixtureDef);

	b2MouseJoint* mouseJoint = nullptr;
	b2BodyDef mouseBodyDef;
	mouseBodyDef.type = b2_kinematicBody;
	b2Body* mouseBody = world.CreateBody(&mouseBodyDef);
	int mx, my;


	float accumulator = 0.0f;
	float fixedDt = 1.0f / 60.0f;
	int velocityIterations = 6;
	int positionIterations = 2;

	float dt = 0.0f;
	Uint32 frameStartTime = SDL_GetTicks();
	Uint32 frameEndTime;

	SDL_Event event;
	bool done = false;
	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_LEFT)
				{
					GPU_Log("Mouse down.\n");

					if (mouseJoint == nullptr)
					{
						// Check to see if a body overlaps with the mouse coordinates
						b2AABB aabb;
						float x = mx / worldToScreenScale;
						float y = my / worldToScreenScale;
						aabb.lowerBound.Set(x - 0.001f, y - 0.001f);
						aabb.upperBound.Set(x + 0.001f, y + 0.001f);

						GetBodyCallback cb(x, y);
						world.QueryAABB(&cb, aabb);
						if (cb.body != nullptr && cb.body != groundBody)
						{
							b2Body* body = cb.body;

							GPU_Log("Creating mouse joint.\n");
							b2MouseJointDef def;
							def.bodyA = groundBody;  //  mouseBody;
							def.bodyB = body;
							def.target = b2Vec2(mx / worldToScreenScale, my / worldToScreenScale);
							def.maxForce = 1000.0f * body->GetMass();
							def.dampingRatio = 0.0f;
							def.collideConnected = true;
							mouseJoint = static_cast<b2MouseJoint*>(world.CreateJoint(&def));
							body->SetAwake(true);
						}
						// Not the best way to find the body!  Instead, you should query overlaps with an AABB (see above).
						/*for (b2Body* body = world.GetBodyList(); body != nullptr; body = body->GetNext())
						{
							if (body->GetType() == b2_dynamicBody)
							{
								for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
								{
									if (fixture->TestPoint(b2Vec2(mx / worldToScreenScale, my / worldToScreenScale)))
									{
										GPU_Log("Creating mouse joint.\n");
										b2MouseJointDef def;
										def.bodyA = groundBody;  //  mouseBody;
										def.bodyB = body;
										def.target = b2Vec2(mx / worldToScreenScale, my / worldToScreenScale);
										def.maxForce = 1000.0f * body->GetMass();
										def.dampingRatio = 0.0f;
										def.collideConnected = true;
										mouseJoint = static_cast<b2MouseJoint*>(world.CreateJoint(&def));
										body->SetAwake(true);
									}
								}
							}
						}*/
					}
				}
			}
			if (event.type == SDL_MOUSEBUTTONUP)
			{
				if (event.button.button == SDL_BUTTON_LEFT)
				{
					if (mouseJoint != nullptr)
					{
						world.DestroyJoint(mouseJoint);
						mouseJoint = nullptr;
					}
				}
			}
		}

		SDL_GetMouseState(&mx, &my);
		my = screen->h - my;
		//mouseBody->SetTransform(b2Vec2(mx/worldToScreenScale, my/worldToScreenScale), 0.0f);
		if (mouseJoint != nullptr)
			mouseJoint->SetTarget(b2Vec2(mx / worldToScreenScale, my / worldToScreenScale));

		while (accumulator >= fixedDt)
		{
			// Instruct the world to perform a single step of simulation.
			// It is generally best to keep the time step and iterations fixed.
			world.Step(fixedDt, velocityIterations, positionIterations);

			accumulator -= fixedDt;
		}


		GPU_ClearRGB(screen, 100, 255, 100);

		// Render all world objects
		for (b2Body* body = world.GetBodyList(); body != nullptr; body = body->GetNext())
		{
			// Now get the position and angle of the body.
			b2Vec2 position = body->GetPosition();
			float32 angle = body->GetAngle() * 180 / (float)M_PI;

			GPU_PushMatrix();
			GPU_Scale(worldToScreenScale, worldToScreenScale, 1.0f);
			GPU_Translate(body->GetPosition().x, body->GetPosition().y, 0.0f);
			GPU_Rotate(body->GetAngle() * 180 / (float)M_PI, 0.0f, 0.0f, 1.0f);
			for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
			{
				b2Shape* shape = fixture->GetShape();
				if (shape->m_type == b2Shape::e_polygon)
				{
					b2PolygonShape* polygon = static_cast<b2PolygonShape*>(shape);
					int n = polygon->m_count;
					b2Vec2* vertices = polygon->m_vertices;

					float scale = powf(worldToScreenScale, -1);  // Once to unscale (without an extra matrix multiply), and once to shrink from screen scale
					DrawTexturedPolygon(smileImage, screen, n, reinterpret_cast<float*>(vertices), 0.0f, 0.0f, scale, -scale);
				}
			}
			GPU_PopMatrix();
		}


		GPU_Flip(screen);

		SDL_Delay(1);

		frameEndTime = SDL_GetTicks();
		dt = (frameEndTime - frameStartTime) / 1000.0f;
		frameStartTime = frameEndTime;

		accumulator += dt;
	}

	GPU_FreeImage(smileImage);
	font.free();

	GPU_Quit();

	return 0;
}