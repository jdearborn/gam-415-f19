#include <iostream>
#include <functional>
using namespace std;


void Print1(int i)
{
	cout << 1 << endl;
}

void Print2(int i)
{
	cout << 2 << endl;
}

void DoFnPrint(void(*fnPtr)(int), int i)
{
	if(fnPtr != nullptr)
		fnPtr(i);
}

int main(int argc, char* argv[])
{
	/*for (int i = 0; i < argc; ++i)
	{
		cout << "Arg " << i << ": " << argv[i] << endl;
	}*/

	Print1(1);
	Print2(1);

	void(*fnPtr)(int);

	fnPtr = Print1;

	fnPtr(1);

	fnPtr = Print2;

	fnPtr(1);


	fnPtr = [](int i) { cout << "3" << endl; };

	fnPtr(1);

	int j = 4;

	std::function<void(int)> otherFn = [j](int i) {cout << j << endl; };

	otherFn(1);


	cin.get();

	return 0;
}