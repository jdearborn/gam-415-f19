#include "GLObjects.h"
#include "SDL.h"
#include "SDL_Image.h"
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <fstream>
using namespace std;


Geometry::Geometry()
	: primitive(GL_TRIANGLES), stride(9)
{}

void Geometry::AddVertex(const Vector3& position, const Color& color, const TexCoord& texCoord)
{
	vertexBuffer.push_back(position.x);
	vertexBuffer.push_back(position.y);
	vertexBuffer.push_back(position.z);

	vertexBuffer.push_back(color.r);
	vertexBuffer.push_back(color.g);
	vertexBuffer.push_back(color.b);
	vertexBuffer.push_back(color.a);

	vertexBuffer.push_back(texCoord.u);
	vertexBuffer.push_back(texCoord.v);
}

std::vector<float>& Geometry::GetBuffer()
{
	return vertexBuffer;
}

void Geometry::Draw()
{

	// Set up vertex/geometry data for interpretation/rendering
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	// Positions
	glVertexAttribPointer(
		0,  // Shader attribute location
		3,  // Number of elements per vertex
		GL_FLOAT,  // Data type
		GL_FALSE,  // Should OpenGL normalize this attribute?
		stride * sizeof(float),  // Stride / extra spacing between elements
		(void*)0  // Offset from the beginning of the buffer
	);

	// Colors
	glVertexAttribPointer(
		1,  // Shader attribute location
		4,  // Number of elements per vertex
		GL_FLOAT,  // Data type
		GL_FALSE,  // Should OpenGL normalize this attribute?
		stride * sizeof(float),  // Stride / extra spacing between elements
		(void*)(3 * sizeof(float))  // Offset from the beginning of the buffer
	);

	// Tex coords
	glVertexAttribPointer(
		2,  // Shader attribute location
		2,  // Number of elements per vertex
		GL_FLOAT,  // Data type
		GL_FALSE,  // Should OpenGL normalize this attribute?
		stride * sizeof(float),  // Stride / extra spacing between elements
		(void*)(7 * sizeof(float))  // Offset from the beginning of the buffer
	);

	glDrawArrays(primitive, 0, vertexBuffer.size() / stride);

	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
}



VertexArrayObject::VertexArrayObject()
{}
VertexArrayObject::~VertexArrayObject()
{
	Free();
}

void VertexArrayObject::Init()
{
	glGenVertexArrays(1, &vao);
}

void VertexArrayObject::Free()
{
	glDeleteVertexArrays(1, &vao);
}

void VertexArrayObject::Bind()
{
	glBindVertexArray(vao);
}



VertexBufferObject::VertexBufferObject()
{}
VertexBufferObject::~VertexBufferObject()
{
	Free();
}

void VertexBufferObject::Init()
{
	glGenBuffers(1, &vbo);
}

void VertexBufferObject::Free()
{
	glDeleteBuffers(1, &vbo);
}

void VertexBufferObject::Bind()
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
}

void VertexBufferObject::Upload(Geometry& geometry)
{
	glBufferData(GL_ARRAY_BUFFER, geometry.GetBuffer().size() * sizeof(float), &geometry.GetBuffer()[0], GL_DYNAMIC_DRAW);
}



ShaderProgram::ShaderProgram()
	: vertexShader(0), fragmentShader(0), program(0)
{}

ShaderProgram::~ShaderProgram()
{
	FreeShaders();
}

void ShaderProgram::FreeShaders()
{
	if (vertexShader > 0)
		glDeleteShader(vertexShader);
	if (fragmentShader > 0)
		glDeleteShader(fragmentShader);
	if (program > 0)
		glDeleteProgram(program);
	vertexShader = 0;
	fragmentShader = 0;
	program = 0;
}

bool ShaderProgram::LoadShaders(const string& vertexShaderFile, const string& fragmentShaderFile)
{
	FreeShaders();

	vertexFile = vertexShaderFile;
	fragmentFile = fragmentShaderFile;

	// Load files into strings
	string vertexString = LoadFileString(vertexShaderFile);
	string fragmentString = LoadFileString(fragmentShaderFile);

	// Allocate shaders in GL driver
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Give GL the source to the shader
	const GLchar* vertexSource = vertexString.c_str();
	glShaderSource(vertexShader, 1, &vertexSource, nullptr);

	const GLchar* fragmentSource = fragmentString.c_str();
	glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);


	// Compile our shaders
	bool vertexResult = CompileShader("Vertex", vertexShader);
	bool fragmentResult = CompileShader("Fragment", fragmentShader);

	// Set up a shader program
	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	int linkStatus;
	glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
	if (linkStatus != GL_TRUE)
	{
		int logLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		char* shaderMessage = new char[logLength + 1];
		glGetProgramInfoLog(program, logLength, nullptr, shaderMessage);

		cout << "Failed to link shader program: " << shaderMessage << endl;
		delete[] shaderMessage;
		return false;
	}

	glUseProgram(program);

	return vertexResult && fragmentResult;
}


bool ShaderProgram::ReloadShaders()
{
	return LoadShaders(vertexFile, fragmentFile);
}

void ShaderProgram::Activate()
{
	glUseProgram(program);
}

GLuint ShaderProgram::GetProgram()
{
	return program;
}

// Returns the entire file loaded into a single string
string ShaderProgram::LoadFileString(const string& filename)
{
	string result;

	ifstream inFile;
	inFile.open(filename);

	if (!inFile.is_open())
	{
		throw std::runtime_error("LoadFileString() failed to open file.");
	}

	string anotherString;
	while (!inFile.eof())
	{
		std::getline(inFile, anotherString);
		result += anotherString + "\n";
	}

	inFile.close();

	return result;
}

bool ShaderProgram::CompileShader(const string& shaderName, GLuint shader)
{
	// Compile shader
	glCompileShader(shader);
	// What happened in that compile step?
	int compileStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
	if (compileStatus != GL_TRUE)
	{
		// Something went wrong!
		// How many bytes in the error message?
		int logLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

		// What is the error message?
		char* shaderMessage = new char[logLength + 1];
		glGetShaderInfoLog(shader, logLength, nullptr, shaderMessage);

		cout << "Failed to compile " << shaderName << " shader: " << shaderMessage << endl;
		delete[] shaderMessage;
		return false;
	}
	return true;
}



static bool has_colorkey(SDL_Surface* surface)
{
	return (SDL_GetColorKey(surface, NULL) == 0);
}

static bool is_alpha_format(SDL_PixelFormat* format)
{
	return SDL_ISPIXELFORMAT_ALPHA(format->format);
}

Texture::Texture()
	: tex(0)
{}
Texture::~Texture()
{
	Free();
}

bool Texture::Load(const string& filename)
{
	Free();

	// Load up the texture
	SDL_Surface* surface = IMG_Load(filename.c_str());
	if (surface == nullptr)
	{
		cout << "Failed to load image (" << filename << "): " << SDL_GetError() << endl;
		return false;
	}

	glGenTextures(1, &tex);
	Bind();

	GLint internalFormat = GL_RGB;
	GLint pixelFormat = GL_BGR;
	// See what the best image format is.
	// WARNING!  This is very fragile and assumes a lot about the format (read: won't work for all images)
	if (surface->format->Amask == 0)
	{
		if (has_colorkey(surface) || is_alpha_format(surface->format))
		{
			internalFormat = GL_RGBA;
			pixelFormat = GL_RGBA;
		}
		else
		{
			internalFormat = GL_RGB;
			pixelFormat = GL_BGR;
		}
	}
	else
	{
		// TODO: Choose the best format for the texture depending on endianness.
		internalFormat = GL_RGBA;
		pixelFormat = GL_RGBA;
	}


	// Send texture data to GPU
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, surface->w, surface->h, 0, pixelFormat, GL_UNSIGNED_BYTE, surface->pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	SDL_FreeSurface(surface);
	return true;
}

void Texture::Free()
{
	if (tex > 0)
		glDeleteTextures(1, &tex);
	tex = 0;
}

void Texture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, tex);  // set this texture as the active one
}

void Texture::UseRepeat()
{
	Bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void Texture::UseClamp()
{
	Bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}




#define FILL_MATRIX_4x4(A, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) \
	A[0] = a0; \
	A[1] = a1; \
	A[2] = a2; \
	A[3] = a3; \
	A[4] = a4; \
	A[5] = a5; \
	A[6] = a6; \
	A[7] = a7; \
	A[8] = a8; \
	A[9] = a9; \
	A[10] = a10; \
	A[11] = a11; \
	A[12] = a12; \
	A[13] = a13; \
	A[14] = a14; \
	A[15] = a15;

Projection::Projection()
{
	SetIdentity();
}

void Projection::SetIdentity()
{
	// Zero out the whole array
	memset(matrix, 0, 16 * sizeof(float));

	// Set diagonals to 1
	matrix[0] = 1.0f;
	matrix[5] = 1.0f;
	matrix[10] = 1.0f;
	matrix[15] = 1.0f;
}

void Projection::SetOrtho(float left, float right, float bottom, float top, float zNear, float zFar)
{
	FILL_MATRIX_4x4(matrix,
		2 / (right - left),				  0,								0,								  0,
		0,								  2 / (top - bottom),				0,								  0,
		0,								  0,								-2 / (zFar - zNear),			  0,
		-(right + left) / (right - left), -(top + bottom) / (top - bottom), -(zFar + zNear) / (zFar - zNear), 1
	);
}

float* Projection::GetMatrix()
{
	return matrix;
}