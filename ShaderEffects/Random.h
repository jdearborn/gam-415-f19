#pragma once

#include <random>
#include <ctime>

class Random
{
private:
	//random_device device;
	std::mt19937 generator;

public:
	Random()
	{
		Seed();
	}
	
	void Seed()
	{
		generator.seed((unsigned int)time(nullptr) + (unsigned int)this);
	}
	
	void Seed(int n)
	{
		generator.seed(n);
	}

	float Range(float min, float max)
	{
		std::uniform_real_distribution<float> dist(min, max);
		return dist(generator);
	}

	int RangeInt(int min, int max)
	{
		std::uniform_int_distribution<int> dist(min, max);
		return dist(generator);
	}
};
