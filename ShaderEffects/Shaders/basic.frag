#version 330 core
in vec4 color;
in vec2 texCoords;
out vec4 outColor;

uniform sampler2D tex;
uniform float time;

void main()
{
	float t = sin(time);
	vec4 texColor = texture2D(tex, texCoords);
	outColor = color * texColor;
    //outColor = vec4(color.r + t, color.g + t, color.b + t, 1.0);
}