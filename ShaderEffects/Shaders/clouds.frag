#version 330 core
in vec4 color;
in vec2 texCoords;
out vec4 outColor;

uniform sampler2D tex;
uniform sampler2D tex2;
uniform float time;

void main()
{
	float t = time;
	// Make the rgb layer move slowly
	vec4 texColor = texture2D(tex, texCoords + vec2(t*0.05, t*0.01));
	// Make the subtraction layer move slowly in a different direction and speed
	vec4 texColor2 = texture2D(tex2, texCoords + vec2(0.5 - t*0.025, 0.5 - t*0.01));
	
	// Use the combined alpha value to remove clouds where the second layer says to.
	outColor = color * vec4(texColor.rgb * texColor.a, texColor.a * texColor2.a);
}