#version 330 core
in vec4 color;
out vec4 outColor;

uniform float time;

void main()
{
	outColor = color;
}