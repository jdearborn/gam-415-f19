#pragma once

#include <cmath>

#ifndef DLL_EXPORT

#ifdef BUILD_LIBRARY
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif

#endif

class DLL_EXPORT Vector3
{
public:
	float x, y, z;

	Vector3()
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
	}

	Vector3(float newX, float newY, float newZ)
		: x(newX), y(newY), z(newZ)
	{}

	Vector3 operator+(Vector3 other)
	{
		Vector3 result;
		result.x = x + other.x;
		result.y = y + other.y;
		result.z = z + other.z;
		return result;
	}

	Vector3& operator+=(Vector3 other)
	{
		x += other.x;
		y += other.y;
		z += other.z;

		return *this;
	}

	Vector3 operator*(float scale)
	{
		Vector3 result;
		result.x = x * scale;
		result.y = y * scale;
		result.z = z * scale;
		return result;
	}

	friend Vector3 operator*(float scale, Vector3 A)
	{
		return A * scale;
	}

	Vector3& operator*=(float scale)
	{
		x *= scale;
		y *= scale;
		z *= scale;

		return *this;
	}

	static float Dot(const Vector3& A, const Vector3& B)
	{
		return A.Dot(B);
	}

	float Dot(const Vector3& other) const
	{
		return x * other.x + y * other.y + z * other.z;
	}

	static Vector3 Cross(const Vector3& A, const Vector3& B)
	{
		return A.Cross(B);
	}

	Vector3 Cross(const Vector3& other) const
	{
		return Vector3(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
	}

	float Length() const
	{
		return sqrtf(x * x + y * y + z * z);
	}

	float Magnitude() const
	{
		return Length();
	}

	void Normalize()
	{
		float len = Length();
		if (len == 0.0f)
			return;
		x /= len;
		y /= len;
		z /= len;
	}

	Vector3 Normalized()
	{
		Vector3 result = *this;
		result.Normalize();
		return result;
	}
};