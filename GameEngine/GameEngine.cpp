#include "GameEngine.h"
#include <iostream>
#include <string>
#include <fstream>
#include "SDL.h"
#include "glew.h"
using namespace std;


// GameEngine static member variables
SimpleEvent GameEngine::OnQuit;
KeyEvent GameEngine::OnKeyDown;
KeyEvent GameEngine::OnKeyUp;

SimpleEvent GameEngine::OnStart;
SimpleEvent GameEngine::OnUpdate;
SimpleEvent GameEngine::OnFixedUpdate;
SimpleEvent GameEngine::OnRender;
bool GameEngine::done = false;
float GameEngine::deltaTime = 0.0f;
float GameEngine::fixedDeltaTime = 0.006f;  // TODO: Remove this and use PhysicsSystem's fixedDeltaTime in GetFixedDeltaTime()

ObjectManager GameEngine::objectManager;
RenderManager GameEngine::renderManager;
// TODO: Put physicsSystem variable instantiation here

Window* GameEngine::window = nullptr;



void PrintAThing()
{
	cout << "A Thing" << endl;
}


string RoundFloat(double input, int decimalPlaces)
{
	string returnMe = "";
	string fullString = to_string(input);
	string decimal = ".";

	returnMe.append(fullString.substr(0, fullString.find(decimal)));
	fullString.erase(0, fullString.find(decimal) + 1);
	returnMe.append(decimal);

	for (int i = 0; i < decimalPlaces; i++)
	{
		char temp = fullString.at(i);
		returnMe.append(1, temp);
	}

	return returnMe;
}


void ArrayToSpreadsheet(uint32_t width, uint32_t height, string** data, string path)
{
	std::ofstream outFile(path);
	for (uint32_t y = 0; y < height; y++)
	{
		for (uint32_t x = 0; x < width; ++x)
		{
			outFile << data[x][y] << ",";
		}
		outFile << "\n";
	}
	outFile.close();
}

string ChangeCase(string aString)
{
	string returnString = "";
	for (auto s : aString)
	{
		char tempChar;
		if (s > 64 && s < 91)
		{
			tempChar = char((int)s + 32);
		}
		else if (s > 96 && s < 123)
		{
			tempChar = char((int)s - 32);
		}
		else
		{
			tempChar = s;
		}
		returnString += tempChar;
	}
	return returnString;
}


Window* GameEngine::Init(const std::string& title, unsigned int width, unsigned int height)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		return nullptr;

	window = new Window(width, height);
	window->SetTitle(title);

	if (!window->IsValid())
	{
		delete window;
		return nullptr;
	}


	renderManager.Init();

	return window;
}

void GameEngine::Deinit()
{
	SDL_Quit();
}


void GameEngine::SetCurrentWindow(Window* w)
{
	window = w;
}


Window* GameEngine::GetCurrentWindow()
{
	return window;
}

float GameEngine::GetDeltaTime()
{
	return deltaTime;
}

float GameEngine::GetFixedDeltaTime()
{
	return fixedDeltaTime;  // TODO: Use physicsSystem's fixedDeltaTime instead
}


void GameEngine::Quit()
{
	done = true;
}


void GameEngine::Run()
{
	for (auto c : *objectManager.toUpdate)
	{
		c->Start();
	}
	OnStart.Invoke();

	// Main loop
	done = false;
	float timeAccumulator = 0.0f;  // TODO: Make this manage the physics updates by doing the TODOs below
	Uint32 frameStartTime = SDL_GetTicks();
	Uint32 frameEndTime;

	SDL_Event event;
	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				done = true;
				OnQuit.Invoke();
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_F4 && (event.key.keysym.mod & KMOD_ALT))
				{
					done = true;
				}
				OnKeyDown.Invoke(static_cast<KeyCode>(event.key.keysym.scancode), static_cast<KeyModifier>(event.key.keysym.mod));
			}
			else if (event.type == SDL_KEYUP)
			{
				OnKeyUp.Invoke(static_cast<KeyCode>(event.key.keysym.scancode), static_cast<KeyModifier>(event.key.keysym.mod));
			}
		}

		OnUpdate.Invoke();
		for (auto c : *objectManager.toUpdate)
		{
			c->Update();
		}

		objectManager.DestroyPending();




		// Loop while we still have enough time to spend on a physics update.  While we do, call FixedUpdate on everything.
		while (timeAccumulator >= fixedDeltaTime)
		{
			OnFixedUpdate.Invoke();
			for (auto c : *objectManager.toUpdate)
			{
				c->FixedUpdate();
			}

			// Consume one chunk of time for this physics update step
			timeAccumulator -= fixedDeltaTime;
		}



		objectManager.DestroyPending();


		renderManager.Render();
		OnRender.Invoke();

		// Wait a bit so we aren't hogging the CPU
		SDL_Delay(1);


		frameEndTime = SDL_GetTicks();
		deltaTime = (frameEndTime - frameStartTime) / 1000.0f;
		frameStartTime = frameEndTime;

		// Add deltaTime into the accumulator
		timeAccumulator += deltaTime;
	}
}


bool Input::GetKey(KeyCode key)
{
	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	if (key != KeyCode::None)
		return keystates[(int)key];

	return false;
}

void ObjectManager::Destroy(Entity* e)
{
	allEntities->erase(e);
	entitiesToDestroy->insert(e);
}

void ObjectManager::Destroy(Component* c)
{
	toUpdate->erase(c);
	componentsToDestroy->insert(c);

	if (c->owner != nullptr)
	{
		c->owner->RemoveComponent(c);
		c->owner = nullptr;
	}
}

void ObjectManager::DestroyImmediate(Entity* e)
{
	allEntities->erase(e);
	delete e;
}

void ObjectManager::DestroyImmediate(Component* c)
{
	toUpdate->erase(c);
	if (c->owner != nullptr)
	{
		c->owner->RemoveComponent(c);
	}
	delete c;
}

void ObjectManager::Clear()
{
	while(allEntities->size() > 0)
	{
		Destroy(*allEntities->begin());
	}
	allEntities->clear();
	toUpdate->clear();
}

void ObjectManager::DestroyPending()
{

}




struct WindowImpl
{
	SDL_Window* window;
};


Window::Window()
{
	windowImpl = new WindowImpl();
	windowImpl->window = nullptr;
}

Window::Window(int width, int height)
	: Window()
{
	windowImpl->window = SDL_CreateWindow("Some window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
		width, height, SDL_WINDOW_OPENGL);

	// Tell SDL which OpenGL version to use
	// Version 3.3 has all of the features we need, but this may need to change if we want
	// to enforce support for later shading language versions.
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GLContext glContext = SDL_GL_CreateContext(windowImpl->window);

	if (glContext == nullptr)
	{
		cout << "Failed to set up OpenGL context: " << SDL_GetError() << endl;
		return;
	}

	GLenum glewStatus = glewInit();
	if (glewStatus != GLEW_OK)
	{
		cout << "Failed to initialize GLEW." << endl;
		return;
	}



	// Make objects which are drawn later avoid drawing parts of themselves that are behind other objects
	//glEnable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_CULL_FACE);

	// glCullFace(GL_FRONT);

	SetClearColor(Color(0.5f, 0.5f, 0.5f, 1.0f));
}

Window::~Window()
{
	SDL_DestroyWindow(windowImpl->window);
	delete windowImpl;
}

void Window::SetTitle(const std::string& title)
{
	SDL_SetWindowTitle(windowImpl->window, title.c_str());
}

bool Window::IsValid()
{
	return (windowImpl->window != nullptr);
}

void Window::SetClearColor(const Color& color)
{
	clearColor = color;
}

void Window::Clear()
{
	glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::SwapToDisplay()
{
	SDL_GL_SwapWindow(windowImpl->window);
}






RenderManager::RenderManager()
{
	allRenderers = new std::set<Renderer*>();
	vao = new VertexArrayObject();

	defaultTexturedShader = new ShaderProgram();
	defaultUntexturedShader = new ShaderProgram();

	shader = defaultTexturedShader;


	projection.SetOrtho(0, 800, 0, 600, -100, 100);

	projectionUniform = new Uniform<float>();
	modelUniform = new Uniform<float>();
}

RenderManager::~RenderManager()
{
	Clear();
	delete allRenderers;
	delete vao;
	delete projectionUniform;
	delete modelUniform;
	delete defaultTexturedShader;
	delete defaultUntexturedShader;
}

void RenderManager::Init()
{
	vao->Init();
	if (!defaultTexturedShader->LoadShaders("basic.vert", "basic.frag"))
	{
		cout << "Failed to load default textured shaders." << endl;
	}
	if (!defaultUntexturedShader->LoadShaders("basic.vert", "basicUntextured.frag"))
	{
		cout << "Failed to load default untextured shaders." << endl;
	}

	vao->Bind();

	SetCurrentShader(shader);
	shader->Activate();
}

void RenderManager::RegisterRenderer(Renderer* c)
{
	allRenderers->insert(c);
}

void RenderManager::Render()
{
	Window* w = GameEngine::GetCurrentWindow();
	if (w == nullptr || shader == nullptr)
		return;

	w->Clear();

	projectionUniform->Set(projection.GetMatrix(), 16);
	projectionUniform->Update();

	for (auto r : *allRenderers)
	{
		r->Render();
	}

	w->SwapToDisplay();
}

void RenderManager::Remove(Renderer* c)
{
	allRenderers->erase(c);
}

void RenderManager::Clear()
{
	allRenderers->clear();
}

void RenderManager::SetCurrentShader(ShaderProgram* shader)
{
	this->shader = shader;
	
	if (shader == nullptr)
		return;

	shader->Activate();
	projectionUniform->LoadLocation(shader->GetProgram(), "projectionMatrix");
	modelUniform->LoadLocation(shader->GetProgram(), "modelMatrix");
}

ShaderProgram* RenderManager::GetCurrentShader()
{
	return shader;
}

ShaderProgram* RenderManager::GetDefaultTexturedShader()
{
	return defaultTexturedShader;
}

ShaderProgram* RenderManager::GetDefaultUntexturedShader()
{
	return defaultUntexturedShader;
}

void RenderManager::SetTransform(Transform* transform)
{
	// Update projection matrix while we're here
	projectionUniform->Set(projection.GetMatrix(), 16);
	projectionUniform->Update();

	// Now update model matrix
	Matrix m;  // Make a temporary matrix

	if (transform != nullptr)
	{
		m = model * transform->GetMatrix();
	}
	else
	{
		m = model;
	}

	modelUniform->Set(m.GetMatrix(), 16);
	modelUniform->Update();
}



SpriteRenderer::SpriteRenderer()
	: Renderer()
{
	AddType(this);
	vbo = new VertexBufferObject();
	vbo->Init();

	quad = new Geometry();
	SetGeometry(100, 100);
	texture = nullptr;
}

SpriteRenderer::~SpriteRenderer()
{
	delete quad;
	delete vbo;
}


void SpriteRenderer::SetTexture(Texture* tex)
{
	texture = tex;
}

void SpriteRenderer::SetGeometry(float width, float height)
{
	quad->Clear();

	float halfW = width / 2;
	float halfH = height / 2;
	quad->AddVertex(Vector3(-halfW, -halfH, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad->AddVertex(Vector3(halfW, -halfH, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad->AddVertex(Vector3(-halfW, halfH, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad->AddVertex(Vector3(-halfW, halfH, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad->AddVertex(Vector3(halfW, -halfH, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad->AddVertex(Vector3(halfW, halfH, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr
}

void SpriteRenderer::Render()
{
	ShaderProgram* oldShader = GameEngine::renderManager.GetCurrentShader();
	GameEngine::renderManager.SetCurrentShader(GameEngine::renderManager.GetDefaultTexturedShader());


	vbo->Bind();
	vbo->Upload(*quad);
	GameEngine::renderManager.SetTransform(owner->GetComponent<Transform>());

	if (texture != nullptr)
	{
		texture->Bind();
	}
	quad->Draw();

	GameEngine::renderManager.SetCurrentShader(oldShader);
}




PolygonRenderer::PolygonRenderer()
	: Renderer()
{
	AddType(this);
	vbo = new VertexBufferObject();
	vbo->Init();

	geometry = new Geometry();
	geometry->SetPrimitive(PrimitiveEnum::LineLoop);
}

PolygonRenderer::~PolygonRenderer()
{
	delete geometry;
	delete vbo;
}


void PolygonRenderer::AddVertex(float x, float y)
{
	geometry->AddVertex(Vector3(x, y, 0.0f), color, TexCoord(0.0f, 0.0f));
}


void PolygonRenderer::SetGeometryAsBox(float w, float h)
{
	float w2 = w / 2;
	float h2 = h / 2;
	AddVertex(-w2, -h2);
	AddVertex(w2, -h2);
	AddVertex(w2, h2);
	AddVertex(-w2, h2);
}

void PolygonRenderer::SetColor(const Color& color)
{
	this->color = color;
}

void PolygonRenderer::Render()
{
	if (geometry->GetNumVertices() < 3)
		return;



	ShaderProgram* oldShader = GameEngine::renderManager.GetCurrentShader();
	GameEngine::renderManager.SetCurrentShader(GameEngine::renderManager.GetDefaultUntexturedShader());

	vbo->Bind();
	vbo->Upload(*geometry);
	GameEngine::renderManager.SetTransform(owner->GetComponent<Transform>());
	geometry->Draw();

	GameEngine::renderManager.SetCurrentShader(oldShader);
}

