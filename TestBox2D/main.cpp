#include <iostream>
#include "GameEngine.h"
#include <functional>
#include <vector>
#include <algorithm>
#include <cassert>
using namespace std;


// TODO: Remove Box2D from this project's include directories, library directories, and linker input dependencies.
// TODO: Move this header and these physics-related classes into the game engine library
#include "Box2D/Box2D.h"

class PhysicsBody2D;

// Represents the physical world and controls how it changes and how it relates to what appears on the screen
// TODO: Hide the Box2D details with a pimpl and by separating the function declarations from the implementations.
class PhysicsSystem
{
public:
	float worldToScreenScale;  // Relates pixels and meters:    pixels per meter
	float worldOffsetX;  // The position of the physical world on the screen
	float worldOffsetY;  // The position of the physical world on the screen

	float fixedDeltaTime;
	int velocityIterations;  // Substeps to make the velocity apply smoother and more accurately
	int positionIterations;  // Substeps to make position changes apply more accurately


	PhysicsSystem();
	~PhysicsSystem();

	void Start();
	void FixedUpdate();
	void RegisterComponent(PhysicsBody2D* body);
	void Clear();


private:
	b2Vec2 gravity;
	b2World* world;
	std::set<PhysicsBody2D*>* allBodies;
};



// TODO: The physics system should not be global here.  It should be moved into the GameEngine class.
PhysicsSystem physicsSystem;

// Represents a definition for how to create a physical piece of geometry that will be attached to a physical body
// TODO: Hide the Box2D details with a pimpl and by separating the function declarations from the implementations.
class Collider2D
{
public:
	Collider2D()
		: shape(nullptr)
	{
		fixtureDef.friction = 0.3f;
		fixtureDef.isSensor = false;
		fixtureDef.restitution = 0.5f;
		fixtureDef.density = 1.0f;
	}

	~Collider2D()
	{
		delete shape;
	}

	void SetAsBox(float w, float h)
	{
		delete shape;
		b2PolygonShape* ps = new b2PolygonShape();
		ps->SetAsBox(w/2 / physicsSystem.worldToScreenScale, h/2 / physicsSystem.worldToScreenScale);
		shape = ps;
		fixtureDef.shape = shape;
	}

	void SetDensity(float d)
	{
		fixtureDef.density = d;
	}

	void SetFriction(float f)
	{
		fixtureDef.friction = f;
	}

	void SetRestitution(float r)
	{
		fixtureDef.restitution = r;
	}

private:
	Collider2D(const Collider2D& other) = delete;
	b2FixtureDef fixtureDef;
	b2Shape* shape;

	b2Fixture* CreateFixture(b2Body* body)
	{
		return body->CreateFixture(&fixtureDef);
	}

	friend class PhysicsBody2D;
};

// Represents a physical object that can have physical geometry attached to it.
// TODO: Hide the Box2D details with a pimpl and by separating the function declarations from the implementations.
class PhysicsBody2D : public Component
{
public:
	PhysicsBody2D()
		: Component(), body(nullptr)
	{
		AddType(this);

		SetDynamic(false);

		physicsSystem.RegisterComponent(this);
	}

	void SetDynamic(bool enable)
	{
		if (body == nullptr)
			bodyDef.type = enable ? b2_dynamicBody : b2_staticBody;
		else
			body->SetType(enable ? b2_dynamicBody : b2_staticBody);
	}

	void SetPosition(float x, float y)
	{
		if (body == nullptr)
			bodyDef.position.Set(x / physicsSystem.worldToScreenScale, y / physicsSystem.worldToScreenScale);
		else
			body->SetTransform(b2Vec2(x / physicsSystem.worldToScreenScale, y / physicsSystem.worldToScreenScale), body->GetAngle());
	}

	void SetRotation(float degrees)
	{
		if (body == nullptr)
			bodyDef.angle = degrees * ((float)3.14159f / 180);
		else
			body->SetTransform(body->GetPosition(), degrees * ((float)3.14159f / 180));
	}

	void AddCollider(Collider2D* collider)
	{
		if (collider == nullptr)
			return;

		colliders.insert(collider);
	}

	void ApplyToTransform(float worldOffsetX, float worldOffsetY, float worldToScreenScale)
	{
		if (body == nullptr)
			return;
		Transform* transform = owner->GetComponent<Transform>();
		if (transform == nullptr)
			return;

		b2Transform t = body->GetTransform();

		transform->position.x = worldOffsetX + t.p.x * worldToScreenScale;
		transform->position.y = worldOffsetY + t.p.y * worldToScreenScale;
		transform->rotation = t.q.GetAngle() * (180 / 3.14159f);
	}

private:
	b2Body* body;
	b2BodyDef bodyDef;

	std::set<Collider2D*> colliders;

	// This is called by the physics system once the scene is all set up so that the physical objects can reflect the same.
	void CreateBody(b2World& world)
	{
		// Call the body factory which allocates memory for the body
		// from a pool and creates the shape (also from a pool).
		// The body is also added to the world.
		body = world.CreateBody(&bodyDef);

		for (auto c : colliders)
		{
			c->CreateFixture(body);
		}
	}

	friend class PhysicsSystem;
};




PhysicsSystem::PhysicsSystem()
	: worldToScreenScale(10.0f), worldOffsetX(0.0f), worldOffsetY(0.0f), fixedDeltaTime(0.006f), velocityIterations(6), positionIterations(2), gravity(0.0f, -9.8f)
{
	allBodies = new std::set<PhysicsBody2D*>();
	world = new b2World(gravity);
}

PhysicsSystem::~PhysicsSystem()
{
	Clear();
	delete allBodies;
}

void PhysicsSystem::Start()
{
	for (auto b : *allBodies)
	{
		b->CreateBody(*world);
	}
}

void PhysicsSystem::FixedUpdate()
{
	// Instruct the world to perform a single step of simulation.
	// It is generally best to keep the time step and iterations fixed.
	world->Step(fixedDeltaTime, velocityIterations, positionIterations);

	for (auto b : *allBodies)
	{
		b->ApplyToTransform(worldOffsetX, worldOffsetY, worldToScreenScale);
	}
}

void PhysicsSystem::RegisterComponent(PhysicsBody2D* body)
{
	allBodies->insert(body);
}

void PhysicsSystem::Clear()
{
	allBodies->clear();
}




















class PlayerController : public Component
{
public:
	PlayerController()
	{
		AddType(this);
	}

	void Update() override
	{
		// TODO: Don't translate the transform.  Instead, AddForce to the physical body in a FixedUpdate function.  Use AddImpulse here for jumping.
		Transform* transform = owner->GetComponent<Transform>();
		if (Input::GetKey(KeyCode::W))
		{
			transform->Translate(0, 1, 0);
		}
		if (Input::GetKey(KeyCode::S))
		{
			transform->Translate(0, -1, 0);
		}
		if (Input::GetKey(KeyCode::A))
		{
			transform->Translate(-1, 0, 0);
		}
		if (Input::GetKey(KeyCode::D))
		{
			transform->Translate(1, 0, 0);
		}
	}
};




// Register with OnKeyDown
void QuitWhenYouPressEscape(KeyCode key, KeyModifier mod)
{
	if (key == KeyCode::Escape)
	{
		GameEngine::Quit();
	}
}

void PrintKeyDown(KeyCode key, KeyModifier mod)
{
	cout << "You pressed: " << (int)key << endl;
}

void PrintKeyUp(KeyCode key, KeyModifier mod)
{
	cout << "You released: " << (int)key << endl;
}




void RunScene()
{
	// Set up event callbacks
	GameEngine::OnQuit += []() {
		cout << "Goodbye!" << endl;
	};

	GameEngine::OnKeyDown += PrintKeyDown;
	GameEngine::OnKeyUp += PrintKeyUp;
	GameEngine::OnKeyDown += QuitWhenYouPressEscape;


	// TODO: Call physicsSystem's Start in game engine library instead of adding it to this callback
	GameEngine::OnStart += []() {
		physicsSystem.Start();
	};
	// TODO: Call physicsSystem's FixedUpdate in game engine library instead of adding it to this callback
	GameEngine::OnFixedUpdate += []() {
		physicsSystem.FixedUpdate();
	};




	// Create ground object
	Entity* ground = Entity::Instantiate();
	ground->AddComponent<Transform>();

	// Set up ground physical body
	PhysicsBody2D* groundBody = ground->AddComponent<PhysicsBody2D>();
	groundBody->SetPosition(400, 5);

	// Set up ground collider
	Collider2D* groundCollider = new Collider2D();
	groundCollider->SetAsBox(400, 10);
	groundBody->AddCollider(groundCollider);

	// Set up ground outline
	PolygonRenderer* groundRenderer = ground->AddComponent<PolygonRenderer>();
	groundRenderer->SetColor(Color(0, 255, 0, 255));
	groundRenderer->SetGeometryAsBox(400, 10);





	// Create a player object and attach the needed components
	Entity* playerObj = Entity::Instantiate();
	Transform* playerTransform = playerObj->AddComponent<Transform>();
	PlayerController* player = playerObj->AddComponent<PlayerController>();

	// Set up player physical body
	PhysicsBody2D* playerBody = playerObj->AddComponent<PhysicsBody2D>();
	playerBody->SetDynamic(true);
	playerBody->SetPosition(400, 200);
	playerBody->SetRotation(20);

	// Set up player collider
	Collider2D* playerCollider = new Collider2D();
	float playerSize = 50;
	playerCollider->SetAsBox(playerSize, playerSize);
	playerCollider->SetDensity(1.0f);
	playerCollider->SetFriction(0.3f);
	playerCollider->SetRestitution(0.5f);
	playerBody->AddCollider(playerCollider);

	// Set up player image (smile)
	SpriteRenderer* playerRenderer = playerObj->AddComponent<SpriteRenderer>();
	Texture t;
	t.Load("Images/smile.png");
	playerRenderer->SetTexture(&t);
	playerRenderer->SetGeometry(playerSize, playerSize);

	// Set up player outline
	PolygonRenderer* playerOutlineRenderer = playerObj->AddComponent<PolygonRenderer>();
	playerOutlineRenderer->SetColor(Color(255, 0, 0, 255));
	playerOutlineRenderer->SetGeometryAsBox(playerSize, playerSize);




	// Start the main loop
	GameEngine::Run();

	// Clean up to prepare for another scene (if there ever is one...)
	GameEngine::OnQuit.Clear();
	GameEngine::OnKeyDown.Clear();
	GameEngine::OnUpdate.Clear();
	GameEngine::OnFixedUpdate.Clear();
	GameEngine::OnKeyUp.Clear();

	// TODO: Add the GameEngine:: scope in front when the physics system is moved to the game engine.
	physicsSystem.Clear();
	GameEngine::objectManager.Clear();
	GameEngine::renderManager.Clear();
}








int main(int argc, char* argv[])
{
	Window* window = GameEngine::Init("Welcome to Game.", 1280, 720);
	if (window == nullptr)
	{
		return 1;
	}

	GameEngine::OnKeyDown += [window](KeyCode k, KeyModifier m) {
		if (k == KeyCode::Space)
		{
			window->SetTitle("You pressed SPACE");
		}
		else if (k == KeyCode::Enter)
		{
			window->SetTitle("You pressed ENTER");
		}
	};


	RunScene();


	// Clean up
	GameEngine::Deinit();

	return 0;
}