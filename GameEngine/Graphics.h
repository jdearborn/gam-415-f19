#pragma once

#include "Vector3.h"
#include <string>
#include <vector>

#ifndef DLL_EXPORT

#ifdef BUILD_LIBRARY
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif

#endif

struct DLL_EXPORT Color
{
public:
	float r, g, b, a;

	Color()
		: r(1.0f), g(1.0f), b(1.0f), a(1.0f)
	{}
	Color(float r, float g, float b)
		: r(r), g(g), b(b), a(1.0f)
	{}
	Color(float r, float g, float b, float a)
		: r(r), g(g), b(b), a(a)
	{}
};

struct DLL_EXPORT TexCoord
{
public:
	float u, v;

	TexCoord()
		: u(0.0f), v(0.0f)
	{}
	TexCoord(float u, float v)
		: u(u), v(v)
	{}
};


enum class PrimitiveEnum
{
	Points = 0x0000,
	Lines = 0x0001,
	LineLoop = 0x0002,
	LineStrip = 0x0003,
	Triangles = 0x0004
};

// Buffer format: x, y, z, r, g, b, a, u, v
class DLL_EXPORT Geometry
{
public:

	Geometry();
	Geometry(const Geometry& other);
	~Geometry();

	void AddVertex(const Vector3& position, const Color& color, const TexCoord& texCoord);

	std::vector<float>& GetBuffer();

	void SetPrimitive(PrimitiveEnum prim);

	void Draw();

	int GetNumVertices() const;

	void Clear();

private:
	PrimitiveEnum primitive;
	int stride;
	std::vector<float>* vertexBuffer;
	int numVertices;
};

class DLL_EXPORT VertexArrayObject
{
public:
	VertexArrayObject();
	~VertexArrayObject();

	void Init();

	void Free();

	void Bind();

private:
	unsigned int vao;

	// Explicitly disable copying this object
	VertexArrayObject(const VertexArrayObject& other) = delete;
	VertexArrayObject& operator=(const VertexArrayObject& other) = delete;
};

class DLL_EXPORT VertexBufferObject
{
public:
	VertexBufferObject();
	~VertexBufferObject();

	void Init();

	void Free();

	void Bind();

	void Upload(Geometry& geometry);

private:
	unsigned int vbo;
};

class DLL_EXPORT ShaderProgram
{
public:
	ShaderProgram();
	ShaderProgram(const ShaderProgram& other) = delete;

	~ShaderProgram();

	void FreeShaders();

	bool LoadShaders(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);

	bool ReloadShaders();

	void Activate();

	unsigned int GetProgram();

private:
	std::string* vertexFile;
	std::string* fragmentFile;
	unsigned int vertexShader, fragmentShader;
	unsigned int program;

	// Returns the entire file loaded into a single string
	std::string LoadFileString(const std::string& filename);

	bool CompileShader(const std::string& shaderName, unsigned int shader);
};




class DLL_EXPORT Texture
{
public:

	Texture();
	~Texture();

	bool Load(const std::string& filename);

	void Free();

	void Bind();

	void UseRepeat();

	void UseClamp();

private:
	unsigned int tex;

	// Explicitly disable copying this object
	Texture(const Texture& other) = delete;
	Texture& operator=(const Texture& other) = delete;
};

class DLL_EXPORT Matrix
{
public:
	Matrix();
	Matrix(const Matrix& other);
	~Matrix();

	Matrix& operator=(const Matrix& other);

	void SetIdentity();
	void SetTranslate(float x, float y, float z);
	void SetScale(float sx, float sy, float sz);
	void SetRotateX(float degrees);
	void SetRotateY(float degrees);
	void SetRotateZ(float degrees);

	void SetOrtho(float left, float right, float bottom, float top, float zNear, float zFar);
	void SetFrustum(float left, float right, float bottom, float top, float zNear, float zFar);
	void SetPerspective(float yFieldOfViewDegrees, float aspect, float zNear, float zFar);

	Matrix operator*(const Matrix& B);
	Matrix& operator*=(const Matrix& B);

	float* GetMatrix();

private:
	float matrix[16];
};


template<typename T>
class Uniform
{
public:

	Uniform()
		: values(nullptr), size(0), program(0), location(-1)
	{
		name = new std::string();
	}
	~Uniform()
	{
		delete[] values;
		delete name;
	}
	Uniform(const Uniform& other)  // Copy constructor
		: values(nullptr), size(0), program(0), location(-1)
	{
		*this = other;  // Reuse the copy assignment operator
	}
	Uniform& operator=(const Uniform& other)  // Copy assignment
	{
		delete[] values;

		program = other.program;
		location = other.location;
		if (other.values == nullptr)
		{
			values = nullptr;
			size = 0;
		}
		else
		{
			size = other.size;
			values = new T[size];
			memcpy(values, other.values, size * sizeof(T));
		}

		return *this;
	}

	void LoadLocation(unsigned int program, const std::string& name);

	void Reload()
	{
		LoadLocation(program, *name);
	}

	void Set(const T& v)
	{
		if (size != 1)
		{
			delete[] values;
			values = new T(v);
			size = 1;
		}
		else
		{
			values[0] = v;
		}
	}

	void Set(const T* newValues, int size)
	{
		if (this->size != size)
		{
			delete[] values;
			values = new T[size];
			this->size = size;
		}
		memcpy(values, newValues, size * sizeof(T));
	}

	// To be specialized for float
	void Set(const Vector3& v);

	// To be specialized
	void Update();

private:

	T* values;
	int size;

	unsigned int program;
	int location;
	std::string* name;
};


template<>
inline void Uniform<float>::Set(const Vector3& v)
{
	Set(&v.x, 3);
}

// Explicitly instantiate these two versions
extern template class Uniform<int>;
extern template class Uniform<float>;
