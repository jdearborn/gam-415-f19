#version 330 core
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec2 inTexCoords;

out vec4 color;
out vec2 texCoords;

uniform float time;
uniform mat4 projectionMatrix;

void main()
{
    gl_Position = projectionMatrix * vec4(vertex.xyz, 1.0);
	color = inColor;
	texCoords = inTexCoords;
}