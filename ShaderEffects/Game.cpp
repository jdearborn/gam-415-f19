#include "Game.h"
#include <iostream>
#include <string>
using namespace std;

int Game::Init()
{
	if (!sdl.Init())
		return 1;

	if (!sdl.CreateWindow("Shader Effects", 800, 600))
		return 2;

	if (!sdl.InitOpenGL(3, 3))
	{
		return 3;
	}

	const GLubyte* versionString = glGetString(GL_VERSION);

	cout << "OpenGL version string: " << versionString << endl;

	basicShader.LoadShaders("Shaders/basic.vert", "Shaders/basic.frag");
	cloudShader.LoadShaders("Shaders/clouds.vert", "Shaders/clouds.frag");
	warpShader.LoadShaders("Shaders/basic.vert", "Shaders/warp.frag");

	glEnable(GL_TEXTURE_2D);


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glViewport(0, 0, sdl.GetW(), sdl.GetH());
	projection.SetOrtho(0, (float)sdl.GetW(), 0, (float)sdl.GetH(), -100, 100);


	return 0;
}

void Game::LoadGeometry()
{
	// top center
	triangle.AddVertex(Vector3(600, 600, 0.0f), Color(1.0f, 0.0f, 0.0f, 1.0f), TexCoord(0.5f, 0.0f));
	// bottom right
	triangle.AddVertex(Vector3(800, 300, 0.0f), Color(0.0f, 1.0f, 0.0f, 1.0f), TexCoord(1.0f, 1.0f));
	// bottom left
	triangle.AddVertex(Vector3(400, 300, 0.0f), Color(0.0f, 0.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));

	quad.AddVertex(Vector3(0.0f, 0.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad.AddVertex(Vector3(100.0f, 0.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(0.0f, 100.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(0.0f, 100.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(100.0f, 0.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(100.0f, 100.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr

	bigQuad.AddVertex(Vector3(200.0f, 100.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	bigQuad.AddVertex(Vector3(600.0f, 100.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	bigQuad.AddVertex(Vector3(200.0f, 500.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	bigQuad.AddVertex(Vector3(200.0f, 500.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	bigQuad.AddVertex(Vector3(600.0f, 100.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	bigQuad.AddVertex(Vector3(600.0f, 500.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr


	// Bind a vertex buffer object to store our geometry data on the GPU
	triangleVBO.Init();
	triangleVBO.Bind();
	// Upload to the GPU
	triangleVBO.Upload(triangle);

	quadVBO.Init();
	quadVBO.Bind();
	quadVBO.Upload(quad);

	bigQuadVBO.Init();
	bigQuadVBO.Bind();
	bigQuadVBO.Upload(bigQuad);
}

int Game::Run()
{

	// Bind vertex array object (container for reusable geometry and its properties)
	vao.Init();
	vao.Bind();
	LoadGeometry();


	// Values for the basic shader
	basicShader.Activate();

	Uniform<float> basicTimeUniform;
	basicTimeUniform.LoadLocation(basicShader.GetProgram(), "time");

	Uniform<float> basicProjectionUniform;
	basicProjectionUniform.LoadLocation(basicShader.GetProgram(), "projectionMatrix");


	// Values for the cloud shader
	cloudShader.Activate();

	Uniform<float> cloudTimeUniform;
	cloudTimeUniform.LoadLocation(cloudShader.GetProgram(), "time");

	Uniform<float> cloudProjectionUniform;
	cloudProjectionUniform.LoadLocation(cloudShader.GetProgram(), "projectionMatrix");



	// Values for the warp shader
	warpShader.Activate();

	Uniform<float> warpTimeUniform;
	warpTimeUniform.LoadLocation(warpShader.GetProgram(), "time");

	Uniform<float> warpProjectionUniform;
	warpProjectionUniform.LoadLocation(warpShader.GetProgram(), "projectionMatrix");




	Texture smileTexture;
	smileTexture.Load("Images/smile.png");
	smileTexture.UseClamp();

	Texture cloudTexture1;
	cloudTexture1.Load("Images/tiling-perlin-noise-alpha.png");
	Texture cloudTexture2;
	//cloudTexture2.Load("Images/vray/vray_osl_alligator_noise_10-alpha.png");
	cloudTexture2.Load("Images/difference-noise-alpha-threshold.png");



	int gameMode = 0;

	// Handle some input
	bool done = false;
	SDL_Event event;
	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				done = true;
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					done = true;
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_R)
				{
					basicShader.ReloadShaders();
					cloudShader.ReloadShaders();
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_W)
				{
					// Move up?
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_SPACE)
				{
					gameMode++;
					if (gameMode > 2)
						gameMode = 0;
				}
			}
		}

		// Do input device state handling here.


		// Do some rendering!
		glClearColor(0.1f, 0.2f, 0.4f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		if (gameMode == 0)
		{
			basicShader.Activate();

			// Uniforms for basic shader
			basicTimeUniform.Set(SDL_GetTicks() / 1000.0f);
			basicTimeUniform.Update();

			basicProjectionUniform.Set(projection.GetMatrix(), 16);
			basicProjectionUniform.Update();

			// Drawing objects with basic shader
			smileTexture.Bind();

			glDisable(GL_BLEND);
			triangleVBO.Bind();
			triangle.Draw();

			glEnable(GL_BLEND);
			quadVBO.Bind();
			quad.Draw();
		}
		else if (gameMode == 1)
		{

			cloudShader.Activate();

			// Uniforms for cloud shader
			cloudTimeUniform.Set(SDL_GetTicks() / 1000.0f);
			cloudTimeUniform.Update();

			cloudProjectionUniform.Set(projection.GetMatrix(), 16);
			cloudProjectionUniform.Update();

			// Drawing objects with cloud shader
			cloudTexture1.Bind();
			cloudTexture2.Bind();

			glEnable(GL_BLEND);
			bigQuadVBO.Bind();
			bigQuad.Draw();
		}
		else if (gameMode == 2)
		{
			warpShader.Activate();

			// Uniforms for warp shader
			warpTimeUniform.Set(SDL_GetTicks() / 1000.0f);
			warpTimeUniform.Update();

			warpProjectionUniform.Set(projection.GetMatrix(), 16);
			warpProjectionUniform.Update();

			// Drawing objects with warp shader
			smileTexture.Bind();

			glEnable(GL_BLEND);
			bigQuadVBO.Bind();
			bigQuad.Draw();
		}

		sdl.SwapWindow();

		SDL_Delay(1);
	}

	return 0;
}