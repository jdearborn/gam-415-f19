#include "SDL.h"
#include <functional>
#include "AudioBuffer.h"
using namespace std;


Sint16 GetSineSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// Figure out how the sample rate relates to real time
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	// Tells us how far along the wave we have traveled
	float waveSegment = sampleIndex / float(samplesPerPeriod);

	return volume * sin(2 * M_PI * waveSegment);
}

Sint16 GetSquareSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// TODO: Implement me!
	return 0;
}

Sint16 GetTriangleSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// TODO: Implement me!
	return 0;
}

Sint16 GetSawtoothSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// TODO: Implement me!
	return 0;
}

Sint16 GetNoiseSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// TODO: Implement me!
	return 0;
}