#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <exception>
using namespace std;

// Returns the entire file loaded into a single string
string LoadFileString(const string& filename)
{
	string result;
	
	ifstream inFile;
	inFile.open(filename);

	if (!inFile.is_open())
	{
		throw std::runtime_error("LoadFileString() failed to open file.");
	}

	string anotherString;
	while (!inFile.eof())
	{
		std::getline(inFile, anotherString);
		result += anotherString + "\n";
	}

	inFile.close();

	return result;
}

int main(int argc, char* argv[])
{
	vector<int> numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

	for (int i = 0; i < numbers.size(); ++i)
	{
		cout << "Element: " << numbers[i] << endl;
	}

	// range-based for loop
	for (int n : numbers)
	{
		cout << "Element: " << n << endl;
	}

	auto j = 0.0f;

	for (vector<int>::iterator e = numbers.begin(); e != numbers.end(); ++e)
	{
		cout << "Using iterator: " << *e << endl;
	}

	//for(auto e = numbers.begin(); e != numbers.end(); ++e)
	//	...

	for (auto n : numbers)
	{
		cout << "Auto with range-based for: " << n << endl;
	}

	try
	{
		string s = LoadFileString("SomeFile.txt");
		cout << "Loaded: " << endl << s << endl;
	}
	catch (std::exception& e)
	{
		cout << "Exception! " << e.what() << endl;
	}


	cin.get();
	return 0;
}