#include <iostream>


#include "Graphics.h"

#include "SDL.h"
#include "glew.h"
#include <string>
using namespace std;



int main(int argc, char* argv[])
{
	// Set up SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
	{
		cout << "SDL failed to initialize! " << SDL_GetError() << endl;
		return 1;
	}

	// Create a window
	SDL_Window* window = SDL_CreateWindow("GAM 415", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL);
	if (window == nullptr)
	{
		cout << "Failed to create window! " << SDL_GetError() << endl;
		return 2;
	}


	// Tell SDL which OpenGL version to use
	// Version 3.3 has all of the features we need, but this may need to change if we want
	// to enforce support for later shading language versions.
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GLContext glContext = SDL_GL_CreateContext(window);

	if (glContext == nullptr)
	{
		cout << "Failed to set up OpenGL context: " << SDL_GetError() << endl;
		return 3;
	}

	GLenum glewStatus = glewInit();
	if (glewStatus != GLEW_OK)
	{
		cout << "Failed to initialize GLEW." << endl;
		return 4;
	}




	// Load shader
	ShaderProgram shader;
	if (!shader.LoadShaders("basic.vert", "basic.frag"))
	{
		cout << "Failed to load shaders." << endl;
		return 5;
	}

	shader.Activate();


	glEnable(GL_TEXTURE_2D);

	// Make objects which are drawn later avoid drawing parts of themselves that are behind other objects
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_CULL_FACE);

	// glCullFace(GL_FRONT);


	// Bind vertex array object (container for reusable geometry and its properties)
	VertexArrayObject vao;
	vao.Init();
	vao.Bind();

	// Bind a vertex buffer object to store our geometry data on the GPU
	VertexBufferObject vbo;
	vbo.Init();
	vbo.Bind();




	Matrix projection;
	projection.SetOrtho(0, 800, 0, 600, -100, 100);

	Matrix model;



	Uniform<float> projectionUniform;
	projectionUniform.LoadLocation(shader.GetProgram(), "projectionMatrix");

	projectionUniform.Set(projection.GetMatrix(), 16);
	projectionUniform.Update();

	Uniform<float> modelUniform;
	modelUniform.LoadLocation(shader.GetProgram(), "modelMatrix");

	Uniform<float> timeUniform;
	timeUniform.LoadLocation(shader.GetProgram(), "time");


	// Define geometry
	Geometry quad;


	quad.AddVertex(Vector3(-50.0f, -50.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad.AddVertex(Vector3(50.0f, -50.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(-50.0f, 50.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(-50.0f, 50.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(50.0f, -50.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, 50.0f, 0.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr


	// Try a cube?
	
	quad.AddVertex(Vector3(-50.0f, -50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad.AddVertex(Vector3(50.0f, -50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(-50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(-50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(50.0f, -50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr

	quad.AddVertex(Vector3(-50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad.AddVertex(Vector3(50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(-50.0f, 50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(-50.0f, 50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, 50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr

	quad.AddVertex(Vector3(50.0f, -50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad.AddVertex(Vector3(50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, 50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr

	quad.AddVertex(Vector3(-50.0f, -50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad.AddVertex(Vector3(-50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(-50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(-50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(-50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(-50.0f, 50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr

	quad.AddVertex(Vector3(-50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad.AddVertex(Vector3(-50.0f, 50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(50.0f, 50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(-50.0f, 50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, 50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr

	quad.AddVertex(Vector3(-50.0f, -50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 1.0f));  // bl
	quad.AddVertex(Vector3(-50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, -50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(50.0f, -50.0f, -50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(0.0f, 0.0f));  // tl
	quad.AddVertex(Vector3(-50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 1.0f));  // br
	quad.AddVertex(Vector3(50.0f, -50.0f, 50.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), TexCoord(1.0f, 0.0f));  // tr
	



	// Submit geometry to GPU
	vbo.Upload(quad);
	



	// Load up the texture
	Texture smileTexture;
	smileTexture.Load("Images/smile.png");




	Matrix rotation;
	Matrix translation;
	translation.SetTranslate(400, 300, 0);

	Matrix smallRotationX;
	smallRotationX.SetRotateX(0.1f);
	Matrix smallRotationY;
	smallRotationY.SetRotateY(0.03f);



	// Handle some input
	bool done = false;
	SDL_Event event;
	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				done = true;
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					done = true;
				}
			}
		}

		// Update some variables every frame
		rotation *= smallRotationX;
		rotation *= smallRotationY;

		model.SetIdentity();
		model = translation * rotation;



		// Clear the screen
		glClearColor(0.4f, 0.0f, 0.4f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		// Update shader variables
		timeUniform.Set(SDL_GetTicks() / 1000.0f);
		timeUniform.Update();

		modelUniform.Set(model.GetMatrix(), 16);
		modelUniform.Update();

		// Render geometry
		smileTexture.Bind();
		vbo.Bind();
		quad.Draw();


		// Show the result to the user
		SDL_GL_SwapWindow(window);


		// Wait a bit so we aren't hogging the CPU
		SDL_Delay(1);
	}

	// Clean up
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);



	SDL_Quit();

	return 0;
}