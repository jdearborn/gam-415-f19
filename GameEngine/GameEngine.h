#pragma once
#include <string>
#include <vector>
#include <functional>
#include <typeinfo>
#include <map>
#include <set>
#include <algorithm>
#include "Vector3.h"

#ifndef DLL_EXPORT

#ifdef BUILD_LIBRARY
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif

#endif

#include "Graphics.h"


DLL_EXPORT void PrintAThing();

DLL_EXPORT std::string RoundFloat(double input, int decimalPlaces);

DLL_EXPORT void ArrayToSpreadsheet(uint32_t width, uint32_t height, std::string** data, std::string path);

DLL_EXPORT std::string ChangeCase(std::string aString);


// Scancodes, technically.  Let's keep it simple.  We can use SDL_SCANCODE_TO_KEYCODE to convert, if needed.  See SDL_scancode.h for more values.
enum class KeyCode
{
	None,
	A = 4,
	D = 7,
	S = 22,
	W = 26,
	Y = 28,
	Return = 40,
	Enter = 40,
	Escape = 41,
	Space = 44,
	Right = 79,
	Left = 80,
	Down = 81,
	Up = 82,
};

enum class KeyModifier
{
	None = 0x0000,
	LShift = 0x0001,
	RShift = 0x0002,
	LCtrl = 0x0040,
	RCtrl = 0x0080,
	LAlt = 0x0100,
	RAlt = 0x0200,
	LGui = 0x0400,
	RGui = 0x0800,
	NumLock = 0x1000,
	CapsLock = 0x2000,
	Mode = 0x4000,
	Reserved = 0x8000
};

// Event class holding callbacks that take no arguments and return nothing
class DLL_EXPORT SimpleEvent
{
private:
	std::vector<std::function<void()>>* callbacks;

public:
	SimpleEvent()
	{
		callbacks = new std::vector<std::function<void()>>();
	}
	~SimpleEvent()
	{
		delete callbacks;
	}

	SimpleEvent& operator+=(std::function<void()> fn)
	{
		callbacks->push_back(fn);
		return *this;
	}

	void Clear()
	{
		callbacks->clear();
	}

	void Invoke()
	{
		for (auto fn : *callbacks)
		{
			fn();
		}
	}
};


// Event class holding callbacks that take a KeyCode and a KeyModifier
class DLL_EXPORT KeyEvent
{
private:
	std::vector<std::function<void(KeyCode, KeyModifier)>>* callbacks;

public:
	KeyEvent()
	{
		callbacks = new std::vector<std::function<void(KeyCode, KeyModifier)>>();
	}
	~KeyEvent()
	{
		delete callbacks;
	}

	KeyEvent& operator+=(std::function<void(KeyCode, KeyModifier)> fn)
	{
		callbacks->push_back(fn);
		return *this;
	}

	void Clear()
	{
		callbacks->clear();
	}

	void Invoke(KeyCode key, KeyModifier mod)
	{
		for (auto fn : *callbacks)
		{
			fn(key, mod);
		}
	}
};


class Entity;
class Component;
class Renderer;

class DLL_EXPORT ObjectManager
{
private:
	std::set<Entity*>* allEntities;
	std::set<Component*>* toUpdate;
	std::set<Entity*>* entitiesToDestroy;
	std::set<Component*>* componentsToDestroy;

	int nextID;

	friend class GameEngine;

public:

	ObjectManager()
		: nextID(1)
	{
		allEntities = new std::set<Entity*>();
		toUpdate = new std::set<Component*>();

		entitiesToDestroy = new std::set<Entity*>();
		componentsToDestroy = new std::set<Component*>();
	}

	~ObjectManager()
	{
		Clear();
		delete allEntities;
		delete toUpdate;
		delete entitiesToDestroy;
		delete componentsToDestroy;
	}

	int GetNextID()
	{
		return nextID++;
	}

	void RegisterEntity(Entity* e)
	{
		allEntities->insert(e);
	}

	void RegisterComponent(Component* c)
	{
		toUpdate->insert(c);
	}

	void Destroy(Entity* e);

	void Destroy(Component* c);

	void DestroyImmediate(Entity* e);
	void DestroyImmediate(Component* c);

	void Clear();

	void DestroyPending();
};

class VertexArrayObject;
class VertexBufferObject;
class ShaderProgram;
class Transform;

class DLL_EXPORT RenderManager
{
private:
	std::set<Renderer*>* allRenderers;
	VertexArrayObject* vao;
	ShaderProgram* defaultTexturedShader;
	ShaderProgram* defaultUntexturedShader;
	ShaderProgram* shader;


	Matrix projection;
	Matrix model;

	Uniform<float>* projectionUniform;
	Uniform<float>* modelUniform;

	friend class GameEngine;

public:

	RenderManager();
	RenderManager(const RenderManager& other) = delete;
	~RenderManager();

	void Init();

	void RegisterRenderer(Renderer* c);

	void Render();

	void Remove(Renderer* c);

	void Clear();

	void SetCurrentShader(ShaderProgram* shader);
	ShaderProgram* GetCurrentShader();
	ShaderProgram* GetDefaultTexturedShader();
	ShaderProgram* GetDefaultUntexturedShader();

	void SetTransform(Transform* transform);
};



class DLL_EXPORT Component
{
public:
	int id;
	std::vector<size_t>* types;

	Entity* owner;

	virtual ~Component()
	{
		delete types;
	}

	template<typename T>
	void AddType(T* obj)
	{
		types->push_back(typeid(*obj).hash_code());
	}

	template<typename T>
	bool HasType()
	{
		size_t value = typeid(T).hash_code();
		for (auto t : *types)
		{
			if (t == value)
				return true;
		}
		return false;
	}

	virtual void Awake()
	{

	}
	virtual void Start()
	{

	}
	virtual void Update()
	{

	}
	virtual void FixedUpdate()
	{

	}

protected:
	Component()
		: id(0), owner(nullptr)
	{
		types = new std::vector<size_t>();
		AddType(this);
	}


	friend class Entity;
};


class Window;

// Class container for static methods to set up and control the game engine
class DLL_EXPORT GameEngine
{
public:
	static SimpleEvent OnQuit;
	static KeyEvent OnKeyDown;
	static KeyEvent OnKeyUp;

	static SimpleEvent OnStart;
	static SimpleEvent OnUpdate;
	static SimpleEvent OnFixedUpdate;
	static SimpleEvent OnRender;

	static ObjectManager objectManager;
	static RenderManager renderManager;
	// TODO: Put PhysicsSystem variable declaration here

	static Window* Init(const std::string& title, unsigned int width, unsigned int height);
	static void Quit();

	static void Run();

	static void Deinit();

	static void SetCurrentWindow(Window* w);
	static Window* GetCurrentWindow();

	static float GetDeltaTime();
	static float GetFixedDeltaTime();

private:

	static bool done;
	static Window* window;
	static float deltaTime;
	static float fixedDeltaTime;  // TODO: Remove this and use PhysicsSystem's fixedDeltaTime in GetFixedDeltaTime()
};


class DLL_EXPORT Transform : public Component
{
public:
	Vector3 position;
	float rotation; // angle in degrees
	//Quaternion rotation;
	Vector3 scale;

	Transform()
		: Component(), rotation(0.0f), scale(1, 1, 1)
	{
		AddType(this);
	}

	void Translate(const Vector3& translation)
	{
		position += translation;
	}

	void Translate(float x, float y, float z)
	{
		position.x += x;
		position.y += y;
		position.z += z;
	}

	void Rotate(float degrees)
	{
		rotation += degrees;
	}

	Matrix GetMatrix() const
	{
		Matrix s;
		Matrix r;
		Matrix t;
		
		r.SetRotateZ(rotation);
		t.SetTranslate(position.x, position.y, position.z);
		s.SetScale(scale.x, scale.y, scale.z);
		return t * r * s;
	}
};

class DLL_EXPORT Renderer : public Component
{
public:
	Renderer()
		: Component()
	{
		AddType(this);

		GameEngine::renderManager.RegisterRenderer(this);
	}

	virtual void Render() = 0;
};


class Geometry;
class Texture;

class DLL_EXPORT SpriteRenderer : public Renderer
{
public:
	SpriteRenderer();
	virtual ~SpriteRenderer();

	virtual void SetTexture(Texture* tex);  // Does not take ownership of this texture: Free it elsewhere.
	virtual void SetGeometry(float width, float height);

	virtual void Render() override;

protected:
	VertexBufferObject* vbo;
	Geometry* quad;
	Texture* texture;
};

class DLL_EXPORT PolygonRenderer : public Renderer
{
public:
	PolygonRenderer();
	virtual ~PolygonRenderer();

	virtual void AddVertex(float x, float y);
	void SetGeometryAsBox(float w, float h);

	void SetColor(const Color& color);

	virtual void Render() override;

protected:
	VertexBufferObject* vbo;
	Geometry* geometry;
	Color color;
};

class Entity
{
private:

	int id;
	std::string name;
	std::string tag;

	// Contain a list of Components
	std::set<Component*> components;
	std::multimap<size_t, Component*> componentRegistry;


	Entity()
		: id(0)
	{}

public:

	static Entity* Instantiate()
	{
		Entity* e = new Entity();
		e->id = GameEngine::objectManager.GetNextID();
		GameEngine::objectManager.RegisterEntity(e);
		return e;
	}

	static void Destroy(Entity* e)
	{
		GameEngine::objectManager.Destroy(e);
	}

	static void DestroyImmediate(Entity* e)
	{
		GameEngine::objectManager.DestroyImmediate(e);
	}

	static void Destroy(Component* c)
	{
		GameEngine::objectManager.Destroy(c);
	}

	static void DestroyImmediate(Component* c)
	{
		GameEngine::objectManager.DestroyImmediate(c);
	}

	template<typename T>
	T* AddComponent()
	{
		T* obj = new T();
		obj->id = GameEngine::objectManager.GetNextID();
		obj->owner = this;

		GameEngine::objectManager.RegisterComponent(obj);

		components.insert(obj);

		for (auto t : *obj->types)
		{
			std::pair<size_t, Component*> p(t, obj);
			componentRegistry.insert(p);
		}

		return obj;
	}

	template<typename T>
	T* GetComponent()
	{
		// How would we identify a derived object with a base type?
		// Here's a workaround for C++'s lack of type reflection...  Make Component store a list of types.
		auto item = componentRegistry.find(typeid(T).hash_code());
		if (item != componentRegistry.end())
			return static_cast<T*>(item->second);
		return nullptr;
	}

	void RemoveComponent(Component* c)
	{
		components.erase(c);
		for (auto e = componentRegistry.begin(); e != componentRegistry.end();)
		{
			if (e->second == c)
			{
				e = componentRegistry.erase(e);
			}
			else
				++e;
		}
	}

	int GetID() const
	{
		return id;
	}

	std::string GetName() const
	{
		return name;
	}

	void SetName(const std::string& newName)
	{
		name = newName;
	}

	std::string GetTag() const
	{
		return tag;
	}

	void SetTag(const std::string& newTag)
	{
		tag = newTag;
	}
};


class DLL_EXPORT Input
{
public:

	static bool GetKey(KeyCode key);
};


struct WindowImpl;

class DLL_EXPORT Window
{
public:
	Window();
	Window(int width, int height);
	~Window();

	void SetTitle(const std::string& title);
	bool IsValid();
	void SetClearColor(const Color& color);
	void Clear();
	void SwapToDisplay();

private:

	Color clearColor;
	// Idiom: pimpl (Private implementation)
	WindowImpl* windowImpl;
};

