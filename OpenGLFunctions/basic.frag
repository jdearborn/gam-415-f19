#version 330 core
in vec4 color;
in vec2 texCoords;
out vec4 outColor;

uniform sampler2D tex;
uniform float time;

void main()
{
	vec4 texColor = texture2D(tex, texCoords);
	outColor = color * texColor;
}