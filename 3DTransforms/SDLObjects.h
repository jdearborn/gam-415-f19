#pragma once
#include "SDL.h"
#include <string>

class SDL
{
public:
	SDL();
	~SDL();

	bool Init();
	bool CreateWindow(const std::string& title, int width, int height);
	bool InitOpenGL(int glMajorVersion, int glMinorVersion);

	SDL_Window* GetWindow() const;
	void SwapWindow();

	int GetW();
	int GetH();

private:
	int width, height;
	SDL_Window* window;
	SDL_GLContext glContext;

	// Explicitly disable copying this object
	SDL(const SDL& other) = delete;
	SDL& operator=(const SDL& other) = delete;
};