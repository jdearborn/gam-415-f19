#include "SDLObjects.h"
#include "glew.h"
#include <string>
#include <iostream>
using namespace std;

SDL::SDL()
	: window(nullptr), glContext(0)
{}

SDL::~SDL()
{
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

bool SDL::Init()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
	{
		cout << "SDL failed to initialize! " << SDL_GetError() << endl;
		return false;
	}
	return true;
}

bool SDL::CreateWindow(const string& title, int width, int height)
{
	this->width = width;
	this->height = height;

	window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
	if (window == nullptr)
	{
		cout << "Failed to create window! " << SDL_GetError() << endl;
		return false;
	}
	return true;
}

bool SDL::InitOpenGL(int glMajorVersion, int glMinorVersion)
{
	// Tell SDL which OpenGL version to use
	// Version 3.3 has all of the features we need, but this may need to change if we want
	// to enforce support for later shading language versions.
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, glMajorVersion);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, glMinorVersion);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	glContext = SDL_GL_CreateContext(window);

	if (glContext == nullptr)
	{
		cout << "Failed to set up OpenGL context: " << SDL_GetError() << endl;
		return false;
	}


	GLenum glewStatus = glewInit();
	if (glewStatus != GLEW_OK)
	{
		cout << "Failed to initialize GLEW." << endl;
		return false;
	}
	return true;
}

SDL_Window* SDL::GetWindow() const
{
	return window;
}

void SDL::SwapWindow()
{
	SDL_GL_SwapWindow(window);
}

int SDL::GetW()
{
	return width;
}

int SDL::GetH()
{
	return height;
}